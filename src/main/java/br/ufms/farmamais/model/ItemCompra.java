/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fred
 */
@Entity
@Table(name = "ITEM_COMPRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemCompra.findAll", query = "SELECT i FROM ItemCompra i"),
    @NamedQuery(name = "ItemCompra.findById", query = "SELECT i FROM ItemCompra i WHERE i.id = :id"),
    @NamedQuery(name = "ItemCompra.findByQuantidade", query = "SELECT i FROM ItemCompra i WHERE i.quantidade = :quantidade"),
    @NamedQuery(name = "ItemCompra.findByValor", query = "SELECT i FROM ItemCompra i WHERE i.valor = :valor"),
    @NamedQuery(name = "ItemCompra.findByIdCompra", query = "SELECT i FROM ItemCompra i WHERE i.idCompra = :idCompra"),
    @NamedQuery(name = "ItemCompra.findByCodigoRemedio", query = "SELECT i FROM ItemCompra i WHERE i.codigoRemedio = :codigoRemedio")})
public class ItemCompra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "QUANTIDADE")
    private int quantidade;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private double valor;
    @Basic(optional = false)
    @Column(name = "ID_COMPRA")
    private int idCompra;
    @Column(name = "CODIGO_REMEDIO")
    private Integer codigoRemedio;

    public ItemCompra() {
    }

    public ItemCompra(Integer id) {
        this.id = id;
    }

    public ItemCompra(Integer id, int quantidade, double valor, int idCompra) {
        this.id = id;
        this.quantidade = quantidade;
        this.valor = valor;
        this.idCompra = idCompra;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public Integer getCodigoRemedio() {
        return codigoRemedio;
    }

    public void setCodigoRemedio(Integer codigoRemedio) {
        this.codigoRemedio = codigoRemedio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemCompra)) {
            return false;
        }
        ItemCompra other = (ItemCompra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufms.farmamais.model.ItemCompra[ id=" + id + " ]";
    }
    
}
