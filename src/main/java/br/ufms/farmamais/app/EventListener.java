package br.ufms.farmamais.app;

import br.ufms.farmamais.controller.AbstractController;
import br.ufms.farmamais.controller.HandleEvent;
import br.ufms.farmamais.model.util.JPAUtil;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Fred
 */
public class EventListener {
    private static class Handler {
        private final AbstractController instance;
        private final Method method;
        
        public Handler(AbstractController instance, Method method) {
            this.instance = instance;
            this.method = method;
        }
        
        public void call(Map<String, Object> data) throws Exception {
            method.invoke(instance, data);
        }
    }
    
    public static EventListener instance = new EventListener();
    
    public static EventListener getInstance() {
        return instance;
    }
    
    private final Map<String, List<Handler>> handlers;
    
    private EventListener() {
        this.handlers = new HashMap<>();
    }
    
    public void registerController(AbstractController controller) {
        Class<?> clazz = controller.getClass();
        
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(HandleEvent.class)) {
                HandleEvent annotation = method.getAnnotation(HandleEvent.class);
                String eventType = annotation.event();
                
                if (eventType != null && !eventType.isEmpty()) {
                    if (!handlers.containsKey(eventType)) {
                        handlers.put(eventType, new ArrayList<Handler>(3));
                    }
                    
                    handlers.get(eventType).add(new Handler(controller, method));
                }
            }
        }
    }
    
    public void fireEvent(String eventType, Map<String, Object> data) {
        List<Handler> handlerList = handlers.get(eventType);
        
        if (handlerList != null) {
            for (Handler handler : handlerList) {
                try {
                    handler.call(data);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
