/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fred
 */
@Entity
@Table(name = "COMPRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c"),
    @NamedQuery(name = "Compra.findById", query = "SELECT c FROM Compra c WHERE c.id = :id"),
    @NamedQuery(name = "Compra.findByValorTotal", query = "SELECT c FROM Compra c WHERE c.valorTotal = :valorTotal"),
    @NamedQuery(name = "Compra.findByFormaPagamento", query = "SELECT c FROM Compra c WHERE c.formaPagamento = :formaPagamento"),
    @NamedQuery(name = "Compra.findByDataRealizacao", query = "SELECT c FROM Compra c WHERE c.dataRealizacao = :dataRealizacao"),
    @NamedQuery(name = "Compra.findByDataEntrega", query = "SELECT c FROM Compra c WHERE c.dataEntrega = :dataEntrega"),
    @NamedQuery(name = "Compra.findByEstoque", query = "SELECT c FROM Compra c WHERE c.estoque = :estoque"),
    @NamedQuery(name = "Compra.findByIdFuncionario", query = "SELECT c FROM Compra c WHERE c.idFuncionario = :idFuncionario")})
public class Compra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "VALOR_TOTAL")
    private double valorTotal;
    @Basic(optional = false)
    @Column(name = "FORMA_PAGAMENTO")
    private String formaPagamento;
    @Basic(optional = false)
    @Column(name = "DATA_REALIZACAO")
    @Temporal(TemporalType.DATE)
    private Date dataRealizacao;
    @Column(name = "DATA_ENTREGA")
    @Temporal(TemporalType.DATE)
    private Date dataEntrega;
    @Basic(optional = false)
    @Column(name = "ESTOQUE")
    private int estoque;
    @Column(name = "ID_FUNCIONARIO")
    private Integer idFuncionario;

    public Compra() {
    }

    public Compra(Integer id) {
        this.id = id;
    }

    public Compra(Integer id, double valorTotal, String formaPagamento, Date dataRealizacao, int estoque) {
        this.id = id;
        this.valorTotal = valorTotal;
        this.formaPagamento = formaPagamento;
        this.dataRealizacao = dataRealizacao;
        this.estoque = estoque;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Date getDataRealizacao() {
        return dataRealizacao;
    }

    public void setDataRealizacao(Date dataRealizacao) {
        this.dataRealizacao = dataRealizacao;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public Integer getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(Integer idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufms.farmamais.model.Compra[ id=" + id + " ]";
    }
    
}
