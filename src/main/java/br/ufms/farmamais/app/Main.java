/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.app;

import java.util.Locale;
import br.ufms.farmamais.controller.ConcluirVendaController;

/**
 *
 * @author Fred
 */
public class Main {

	public static void main(String[] args) {
		Locale.setDefault(new Locale("pt","BR"));
		new ConcluirVendaController();
	}

}
