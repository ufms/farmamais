/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.controller;

import br.ufms.farmamais.model.Cliente;
import br.ufms.farmamais.model.ItemVenda;
import br.ufms.farmamais.model.Remedio;
import br.ufms.farmamais.model.Venda;
import br.ufms.farmamais.view.ConcluirVenda;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author Fred
 */
public class ConcluirVendaController extends AbstractController {
    
    private final ConcluirVenda view;
    private ArrayList<Object> listagemRemedios;
    
    public ConcluirVendaController() {
        view = new ConcluirVenda();
        view.setVisible(true);
    }
    
    @HandleEvent(event = "buscarClientes")
    public void buscarClientes(Map<String, Object> data) {
        BigInteger documentoPessoal = new BigInteger(data.get("documentoPessoal").toString());
        final List<Cliente> clientes = this.getEntityManager().createNamedQuery("Cliente.findByDocumentoPessoal")
            .setParameter("documentoPessoal", documentoPessoal).getResultList();
        
        updateView(new Runnable() {
            @Override
            public void run() {
                view.infoCliente(clientes.get(0));
            }
        });
    }
    
    @HandleEvent(event = "buscarRemedioPorCodigo")
    public void buscarRemedioPorCodigo(Map<String, Object> data) {
        Integer codigo = new Integer(data.get("codigo").toString());
        final List<Remedio> remedios = this.getEntityManager().createNamedQuery("Remedio.buscarRemedioPorCodigo")
            .setParameter("codigo", codigo).getResultList();
        
        updateView(new Runnable() {

            @Override
            public void run() {
                view.inserirRemedio(remedios.get(0));
            }
        });
    }
    
    @HandleEvent(event = "concluirVenda")
    public void concluirVenda(Map<String, Object> data) {
        try {
            Venda venda = new Venda();
            Vector remedios = (Vector) data.get("remedios");
            Double total = 0.0;
            this.getEntityManager().getTransaction().begin();

            for (int i =0 ; i > remedios.size(); i++) {
                ItemVenda itemVenda = new ItemVenda();
//                1, remedios[i], null, remedios[i][0]);
//                total += (Double) remedios.get(i)[2];
                
//                this.getEntityManager().persist(itemVenda);
            }
            
            venda.setDataRealizacao(new Date());
            venda.setValorTotal(total);
            venda.setFormaPagamento(data.get("formaPagamento").toString());

            this.getEntityManager().persist(venda);
            
            this.getEntityManager().getTransaction().commit();
        } catch(Exception ex) {
//            this.getEntityManager().getTransaction().rollback();
        }
    }
}
