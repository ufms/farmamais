/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fred
 */
@Entity
@Table(name = "ITEM_VENDA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ItemVenda.findAll", query = "SELECT i FROM ItemVenda i"),
    @NamedQuery(name = "ItemVenda.findById", query = "SELECT i FROM ItemVenda i WHERE i.id = :id"),
    @NamedQuery(name = "ItemVenda.findByQuantidade", query = "SELECT i FROM ItemVenda i WHERE i.quantidade = :quantidade"),
    @NamedQuery(name = "ItemVenda.findByValor", query = "SELECT i FROM ItemVenda i WHERE i.valor = :valor"),
    @NamedQuery(name = "ItemVenda.findByIdVenda", query = "SELECT i FROM ItemVenda i WHERE i.idVenda = :idVenda"),
    @NamedQuery(name = "ItemVenda.findByCodigoRemedio", query = "SELECT i FROM ItemVenda i WHERE i.codigoRemedio = :codigoRemedio")})
public class ItemVenda implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "QUANTIDADE")
    private double quantidade;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private double valor;
    @Basic(optional = false)
    @Column(name = "ID_VENDA")
    private int idVenda;
    @Basic(optional = false)
    @Column(name = "CODIGO_REMEDIO")
    private int codigoRemedio;

    public ItemVenda() {
    }

    public ItemVenda(Integer id) {
        this.id = id;
    }

    public ItemVenda(Integer id, double quantidade, double valor, int idVenda, int codigoRemedio) {
        this.id = id;
        this.quantidade = quantidade;
        this.valor = valor;
        this.idVenda = idVenda;
        this.codigoRemedio = codigoRemedio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(int idVenda) {
        this.idVenda = idVenda;
    }

    public int getCodigoRemedio() {
        return codigoRemedio;
    }

    public void setCodigoRemedio(int codigoRemedio) {
        this.codigoRemedio = codigoRemedio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemVenda)) {
            return false;
        }
        ItemVenda other = (ItemVenda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufms.farmamais.model.ItemVenda[ id=" + id + " ]";
    }
    
}
