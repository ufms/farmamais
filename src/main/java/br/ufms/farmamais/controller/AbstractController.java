/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.controller;
import br.ufms.farmamais.app.EventListener;
import br.ufms.farmamais.model.util.JPAUtil;
import javax.persistence.EntityManager;
import javax.swing.SwingUtilities;

/**
 *
 * @author Fred
 */
public abstract class AbstractController {
    private EntityManager entityManager;
    
    public AbstractController(){
        EventListener.getInstance().registerController(this);
    }
    
    public EntityManager getEntityManager () {
        if (entityManager == null) {
            entityManager = JPAUtil.createEntityManager();
        }
        return entityManager;
    }
    
    public void cleanUpEntityManger() {
        if (getEntityManager().isOpen()) {
            getEntityManager().close();
        }
    }
    
    protected void updateView(Runnable runnable) {
        SwingUtilities.invokeLater(runnable);
    }
}
