/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufms.farmamais.model.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Fred
 */
public class JPAUtil {

    /** Unidade de persistencia definida no arquivo <code>persistence.xml</code> */
    private static final String PERSISTENCE_UNIT_NAME = "farmamaisPU";

    private static EntityManagerFactory emf;

    static {
        try {
            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    /**
     * @return Cria e retorna um componente <code>EntityManager</code>.
     * @throws Lança <code>RuntimeException</code> se <code>EntityManagerFactory</code> estiver fechada. 
     */
    public static EntityManager createEntityManager() {
    	if (!emf.isOpen())
    		throw new RuntimeException("EntityManagerFactory está fechada!");
    	return emf.createEntityManager();
    }
    
    /**
     * Fecha o <code>EntityManagerFactory</code>.
     */
    public static void closeEntityManagerFactory() {
    	if (emf.isOpen()) {
    		emf.close();
    	}
    }

}
